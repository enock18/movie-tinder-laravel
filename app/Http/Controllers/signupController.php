<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Users;

class signupController extends Controller
{
   
    public function createUser(Request $request)
    {
        
         return view('signup');
       
    } 
    public function userForm(Request $request)
    {
        $post=new Users();
        $post->email = $request->email;
        $post->name = $request->name;
        $post->password = $request->password;
        $post->role = $request->role;
        $post->save();
        return view('signup');

        
    }
    
}
