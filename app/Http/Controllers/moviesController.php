<?php

namespace App\Http\Controllers;
use App\Models\Movies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class moviesController extends Controller
{
    public function showMovies()
    {
         $movies= Movies::all();
        return view('index')->with('movies', $movies);
    }
}
