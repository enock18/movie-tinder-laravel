# TinderMovie - Backend Laravel

## Installation

1. Lancer `composer install`
2. Copier .env.example en .env
3. Mettre les infos de connexion à la base de données
4. Lancer `php artisan key:generate`
5. Lancer les mirations `php artisan migrate`
