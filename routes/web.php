<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\signController;
use App\Http\Controllers\usersController;
use App\Http\Controllers\moviesController;
use App\Http\Controllers\signupController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[moviesController::class, 'showMovies']);
Route::get('/users', [usersController::class, 'showUsers']);
Route::get('/signup', [signupController::class, 'createUser'])->name("login.create");
Route::post('/signup', [signupController::class, 'userForm'])->name("login.form");
Route::post('/signin', [signController::class, 'authenticate'])->name("signin.auth");
Route::get('/signin', [signController::class, 'showSign'])->name("do.auth");
Route::get('/index', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');
