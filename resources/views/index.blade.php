@extends('welcome')

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Document</title>
</head>


<body>

    <nav class="navbar navbar-light bg-light fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">WORK-SPACE</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar" id="burger">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Offcanvas</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">

                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                            <button type="button" class="btn btn-outline-dark btn-rounded" data-mdb-ripple-color="dark">
                                Home
                            </button>


                        </li>
                        <li class="nav-item">

                            <button type="button" class="btn btn-outline-dark btn-rounded" data-mdb-ripple-color="dark">
                                Link
                            </button>

                        </li>
                        <li class="nav-item dropdown">
                            <button type="button" class="btn btn-outline-dark btn-rounded" data-mdb-ripple-color="dark">
                                drop
                            </button>

                        </li>
                    </ul>
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </div>
    </nav>

    <div class="btn-group" id="filter_backoffice">
        <button type="button" class="btn btn-primary">filtrer par</button>
        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-mdb-toggle="dropdown" aria-expanded="false">
            <span class="visually-hidden">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Titre du film</a></li>
            <li><a class="dropdown-item" href="#"> Nom d'acteur</a></li>
            <li><a class="dropdown-item" href="#"> Nom du réalisateur</a></li>
            <li>
                <hr class="dropdown-divider" />
            </li>
            <li><a class="dropdown-item" href="#">Maison de prod</a></li>
        </ul>
    </div>


    <div class="container">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            @foreach ( $movies as $movie)
            <div class="col">
               
                <div class="card h-100">
                    <img src="https://mdbcdn.b-cdn.net/img/new/standard/city/044.webp" class="card-img-top" alt="Skyscrapers" />
                    <div class="card-body">
                    
                        
                    
                        <h5 class="card-title">titre {{  $movie->titre }}</h5>
                        <p class="card-text">
                            Synopsis:{{$movie->synopsis}}
                        </p>
                        <p class="card-text">
                            {{-- Date Released:{{ $movie->date }} --}}
                        </p>
                        <p class="card-text">
                            {{-- Duration:{{ $movie->duration }} --}}
                        </p>
                        {{-- <p class="card-text">
                            Genre:
                        </p> --}}
               
                    <div>
                            <form action="">
                                <button type="button" class="btn btn-outline-success btn-floating" data-mdb-ripple-color="dark">
                                    <i class="far fa-thumbs-up" type="submit"></i>
                                </button>

                                <button type="button" class="btn btn-outline-success btn-floating" data-mdb-ripple-color="dark" type="submit">
                                    <i class="fas fa-share-alt"></i>
                                </button>

                                <button type="button" class="btn btn-outline-success btn-floating" data-mdb-ripple-color="dark">
                                    <i class="far fa-thumbs-down" type="submit"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                 
                    <div class="card-footer">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                </div>
               
            </div>
            @endforeach
        </div>
    </div>

    
</body>

</html>