@extends('welcome')

<div style="margin-buttom:100px" class="container py-5 h-100">
    <!-- Success message -->
    @if(Session::has('success'))
    <div class="alert alert-success">
        Session::get('success')
    </div>
    @endif
    <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-8 col-lg-6 col-xl-5">
            <div class="card bg-dark text-white" style="border-radius: 1rem;">
                <div class="card-body p-5 text-center">

                    <div class="mb-md-5 mt-md-4 pb-5">

                        <h2 class="fw-bold mb-2 text-uppercase">Login</h2>
                        <p class="text-white-50 mb-5">Please enter your login and password!</p>
                        <form method="POST"  action=" {{ route('login.create') }}">
                           @csrf
                                <div class="form-outline form-white mb-4">
                                    <input type="email" id="typeEmailX" class="form-control form-control-lg  $errors->has('email') ? 'error' : '' " name="email" />
                                    <label class="form-label" for="typeEmailX">Email</label>
                                    @if ($errors->has('email'))
                                    <div class="error">
                                         $errors->first('email') 
                                    </div>
                                    @endif
                                </div>

                                <div class="form-outline form-white mb-4">
                                    <input type="text" id="typePasswordX" class="form-control form-control-lg" name="name" />
                                    <label class="form-label" for="typePasswordX">name</label>
                                </div>
                                <div class="form-outline form-white mb-4">
                                    <input type="password" id="typePasswordX" class="form-control form-control-lg" name="" />
                                    <label class="form-label" for="typePasswordX">Password</label>
                                </div>
                                <div class="form-outline form-white mb-4">
                                    <input type="password" id="typePasswordX" class="form-control form-control-lg" name="password" />
                                    <label class="form-label" for="typePasswordX">Confirm your password</label>
                                </div>

                                <div class="form-outline form-white mb-4">
                                    <input type="text" id="typePasswordX" class="form-control form-control-lg" name="role" />
                                    <label class="form-label" for="typePasswordX">role</label>
                                </div>
                       
                        <p class="small mb-5 pb-lg-2"><a class="text-white-50" href="#!">Forgot password?</a></p>

                        <button class="btn btn-outline-light btn-lg px-5" type="submit">Login</button>
                    </form>
                        <div class="d-flex justify-content-center text-center mt-4 pt-1">
                            <a href="#!" class="text-white"><i class="fab fa-facebook-f fa-lg"></i></a>
                            <a href="#!" class="text-white"><i class="fab fa-twitter fa-lg mx-4 px-2"></i></a>
                            <a href="#!" class="text-white"><i class="fab fa-google fa-lg"></i></a>
                        </div>



                    </div>

                    <div>
                        <p class="mb-0">Don't have an account? <a href="#!" class="text-white-50 fw-bold">Sign Up</a></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
{{-- @section('contenu')
    <form action="/connexion" method="post" class="section">
        {{ csrf_field() }}

        <div class="field">
            <label class="label">Adresse e-mail</label>
            <div class="control">
                <input class="input" type="email" name="email" value="{{ old('email') }}">
            </div>
            @if($errors->has('email'))
                <p class="help is-danger">{{ $errors->first('email') }}</p>
            @endif
        </div>

        <div class="field">
            <label class="label">Mot de passe</label>
            <div class="control">
                <input class="input" type="password" name="password">
            </div>
            @if($errors->has('password'))
                <p class="help is-danger">{{ $errors->first('password') }}</p>
            @endif
        </div>

        <div class="field">
            <div class="control">
                <button class="button is-link" type="submit">Se connecter</button>
            </div>
        </div>
    </form>
@endsection --}}