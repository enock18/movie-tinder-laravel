@extends('welcome')
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div class="input-group" id="search">
        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
        <button type="button" class="btn btn-outline-primary">search</button>
    </div>
    <div>
       
        <table class="table table-hover table-dark" id="user">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">email</th>
                    <th scope="col">Handle</th>
                    {{-- <th scope="col">Options</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                 
                    <td>{{$user->name}}</td> 
                    <td>{{$user->email}}</td>
                    <td> 
                        <button
                        type="button"
                        class="btn btn-outline-danger btn-rounded"
                        data-mdb-ripple-color="dark">
                        Bloquer
                        </button>  
                        <button
                        type="button"
                        class="btn btn-outline-success btn-rounded"
                        data-mdb-ripple-color="dark">
                        Debloquer
                        </button>
                        <button type="button" class="btn btn-danger">Supprimer</button>
                    </td>
                  
                </tr>

                @endforeach
            
            </tbody>
        </table>
    
    </div>

</body>

</html>